package com.bizao.gestionvol.controllers;

import com.bizao.gestionvol.dto.CompanyDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
class CompanyControllerTest
{

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void givenCompanies_ShouldReturnCompaniesList() {
        String baseUrl = "http://localhost:" + port + "/gestionvol/v1/company/list";

        ResponseEntity<CompanyDTO[]> response = restTemplate.getForEntity(baseUrl, CompanyDTO[].class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}
